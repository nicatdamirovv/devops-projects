#!/bin/bash

while true; do
    # Wait for a connection and read the request
    { 
        echo -ne "HTTP/1.1 200 OK\r\nContent-Length: $(date +%T | wc -c)\r\n\r\n$(date +%T) "; 
    } | nc -l -p 8080 -q 1 > request.txt
    
    # Read the request
    request=$(cat request.txt)
    
    # Check if the request is for /date
    if [[ $request == *"GET /date"* ]]; then
        # Get the current time and second
        current_time=$(date +%T)
        second=$(date +%S)
        
        # Determine if the second is odd or even
        if (( second % 2 == 0 )); then
            result="EVEN"
        else
            result="ODD"
        fi
        
        # Prepare the response
        response="$current_time $result"
        echo -ne "HTTP/1.1 200 OK\r\nContent-Length: ${#response}\r\n\r\n$response" | nc -l -p 8080 -q 1
    else
        # Respond with 404 Not Found for any other request
        echo -ne "HTTP/1.1 404 Not Found\r\nContent-Length: 0\r\n\r\n" | nc -l -p 8080 -q 1
    fi
done
